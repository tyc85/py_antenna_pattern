 Python antenna pattern parse and plot  project
=======================

This is a project created by Tsung-Yi Chen @ Spidercloud Wireless.
The purpose of this project is to allow anyone to import a antenna pattern
in planet file format and generate a pdf or eps plot from the file. 

----

This is the README file for the project. Currently the knobs of the plot include
the angle offset (in unit of degrees) and the file format (either eps or pdf)
