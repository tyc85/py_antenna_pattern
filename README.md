 Python antenna pattern parse and plot  project
=======================

This is a project created by Tsung-Yi Chen @ Spidercloud Wireless.
The purpose of this project is to allow anyone to import a antenna pattern
in planet file format and generate a pdf or eps plot from the file. 

----

This is the README file for the project. Currently the knobs of the plot include
the angle offset (in unit of degrees) and the file format (either eps or pdf)

# HOW-TO:
  - Clone the repo to your local folder:git clone https://tsungyichen@bitbucket.org/scw_systems/py_antenna_pattern.git 
  - cd into the repo folder
  - Install from the repo using pip (use sudo if you are not using virtualenv):
    ```python
    pip install -e ./
    ```
  - Create a file list of your planet files: ls /home/name/file_folder/freq/*.txt > /home/name/your_folder/file_list
  - To start using with interactive python:
  ```
  >>> from py_antenna_pattern.pyap import Pyap
  >>> pyap = Pyap('/home/name/your_folder/file_list')
  >>> pyap.polar_pattern()
  ```

