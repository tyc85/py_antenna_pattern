__author__ = 'tchen'

import re
import pdb
import py_antenna_pattern.config as config


def parse_freq_band(file_name):
    pattern = re.compile(r'''
        \s*                # skip the leading spaces
        (?P<leading>\S+?)_   
        (?P<ant_num>\S+?)_  # 0 or more digit non-greedy match: (e.g., ant1)
        .+?                # whatever that is in between, non greedy match
        (?P<band>\d+)      # the trailing band with 3 - 4 digits
        \S*                # rest of the string
    ''', re.VERBOSE)
    
    m = pattern.match(file_name)
    if m == None:
        print('matching frequency band failed\n')
        raise

    if config.VERBOSE is True:
        print('file name parsing yield:', 
              [m.group('leading'), m.group('ant_num'), m.group('band')])
    
    return m.group('band')
           
'''
def parse_data_by_ant(file_name):
    # return values
    parser = Parser()
    if config.VERBOSE is True:
        print('opening file ', file_name)

    try:
        with open(file_name, 'r') as fp:
            for line in fp:
                parser.parse_line(line) 
    except FileNotFoundError:
        # try path without the project name 
        with open(file_name.replace('py_antenna_pattern/', ''), 'r') as fp:
            for line in fp:
                parser.parse_line(line) 
        
    
    try:
        result = {'vertical': parser.rho_v,
                  'horizontal': parser.rho_h,
                  'max_gain_db': parser.max_gain_db,
                  'max_gain_db_str': parser.max_gain_db_str, 
                  'frequency': parser.frequency}
    except:
        print('Exception for assigning result dictionary')
        raise

    return result
'''
